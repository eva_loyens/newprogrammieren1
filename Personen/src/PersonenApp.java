import org.campus.personen.Person;

public class PersonenApp
{

		public static void main (String[] args) //mann k�nnte auch "parameter" schreiben statt "args"
		//"args" ist nur name vom parameter
		
		{
			Person p1 = new Person("Luke", "Skywalker");
			System.out.println(p1);
			
			Person p2 = new Person("Anakin", "Skywalker");
			System.out.println(p2);
			
			Person p3 = new Person ("Padme", "Amidala");
			Person p4 = new Person ("Obiwan", "Kenobi");
						
			p1.setVater(p2); //"Luke, ich bin dein Vater" /"hhch chch huhuhch"
			p1.setMutter(p3);
					
			System.out.println(p1);
			System.out.println(p4);
		}
}
