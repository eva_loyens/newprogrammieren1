package org.campus.personen;

public class Person  // Konstruktor methode programmieren
{
	private String vorname;
	private String nachname;
	private Person mutter;
	private Person vater;
	
	private static int anzahlPersonen;
	
	public Person (String vorname, String nachname)
	{
		this.vorname = vorname;	// mann k�nnte auch "self" schreiben aber "this" ist bei java standard
		// f�r java ist dann klar das es f�r das eigene objekt zutrifft
		// achte auf die farbe
		this.nachname = nachname;
		anzahlPersonen++;
	}
	
	public void setVater(Person vater)
	{
		this.vater = vater;
	}
	
	public void setMutter(Person mutter) // "set": ich teile dir meine mutter mit
	// set und get sind auch eine methode 
	// es gibt auch "get": ich frage den wert ab
	{
		this.mutter = mutter;
	}

	public String toString ()
	
	{	// %d --> d von digit // %s --> s von string
		return String.format("(%s %s (%s %s)) %d", vorname, nachname, mutter, vater, anzahlPersonen);
		//"Vorname: " + vorname + " Nachname: " + nachname;
	}
}
