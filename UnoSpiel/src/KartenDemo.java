

import org.campus.uno.Farbe;
import org.campus.uno.Karte;

public class KartenDemo

{
	public static void main (String[] args)

	{
		Karte k1 = new Karte(Farbe.rot, 2); // konstruktror initialisiert
		Karte k2 = new Karte(Farbe.gelb, 3);
		Karte k3 = new Karte(Farbe.rot, 4);
		Karte k4 = new Karte(Farbe.blau, 4);
		Karte k5 = new Karte(Farbe.blau, 6);
		
		System.out.println(k1);
		System.out.println(k2);
		System.out.println(k3);
		System.out.println(k4);
		System.out.println(k5);
		System.out.println();
		
		System.out.println(k1.match(k2));
		System.out.println(k2.match(k3));
		System.out.println(k3.match(k4));
		System.out.println(k4.match(k5));
		System.out.println();
		
		System.out.println(k1.compare(k3));
		System.out.println(k4.compare(k5));
		
	}
}