package org.campus.uno;
import java.util.ArrayList;

public class Spieler
{
	private String name;
	
	private ArrayList<Karte> handkarten = new ArrayList<Karte>();
	
	public Spieler (String name) // konstruktor Spieler
	{
		this.name = name;
	}
	
	public void aufnehmen (Karte karte)
	{
		handkarten.add(karte);
	}
	
	public String toString()
	{
		return name + " " + handkarten;
	}
	
	public Karte passendeKarte(Karte vergleich)
	{
		for (Karte karte : handkarten)
		{
			if (karte.match(vergleich))
			{
				handkarten.remove(karte);
				if (handkarten.size() == 1)
				{	
					System.out.println(" sagt: UNO ");
				}
				return karte;
			}
		}
		return null;
	}
	
	public int anzahlDerHandkarten()
	{
		return handkarten.size(); //size ist eine methode von der arraylist // gibt die l�nge von der arrayliste zur�ck
	}
	
}

