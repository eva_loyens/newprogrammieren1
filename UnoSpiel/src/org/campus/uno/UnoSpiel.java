package org.campus.uno;
import java.util.ArrayList;
import java.util.Collections;

public class UnoSpiel
{
	private ArrayList<Karte> ablageStapel = new ArrayList<Karte>();
	private ArrayList<Karte> kartenStapel = new ArrayList<Karte>();
	
	private ArrayList<Spieler> mitspieler = new ArrayList<Spieler>();
		
	public UnoSpiel()
	{
		for(int index = 0; index < 2; index++) // er macht von jeder farbe 2x die karten
		{
			for (int zahlenwert = 0; zahlenwert < 10; zahlenwert++)
			{	
				kartenStapel.add(new Karte(Farbe.rot, zahlenwert));
				kartenStapel.add(new Karte(Farbe.blau, zahlenwert));
				kartenStapel.add(new Karte(Farbe.gelb, zahlenwert));
				kartenStapel.add(new Karte(Farbe.gruen, zahlenwert));
			}
		}
		// shuffle kursiv heist es ist statisch
		
		Collections.shuffle(kartenStapel); // karten mischen
	}
	
	public Karte abheben() // liefert eine Karte zur�ck
	{	
		// wir k�nnten auch checken: sind noch genug karten am stapel?
		if(kartenStapel.size() ==0)
		{	
			// oberste karte merken
			Karte obersteKarte = ablageStapel.remove(0);
			Collections.shuffle(ablageStapel); // restlichen stapel mischen
			
			// Aus ablagestapel neuen kartenstapel machen
			kartenStapel.addAll(ablageStapel); // alle Karten einf�gen
			
			ablageStapel.clear(); // kartestapel leer und
			ablageStapel.add(obersteKarte); // oberste karte wieder drauf legen
		
		}
		return kartenStapel.remove(0); // remove an der stelle 0
	}
	
	public void karteAblegen (Karte ablegeKarte) // void weil diese methode nichts zur�ck liefert
	{
		ablageStapel.add(0, ablegeKarte); // 0 heisst die karte komt oben drauf/wird eingef�gt an stelle 0, an stelle 0 im ArrayList
	}
	
	public void mitSpielen(Spieler neuerSpieler)
	{
		mitspieler.add(neuerSpieler);
	}
	
	public boolean spielzug()
	{
		//wer ist dran?
		//wir nehmen ihn von der liste raus, en f�gen ihn am schluss wieder zu
		Spieler aktuellerSpieler = mitspieler.remove(0);
		System.out.println("am Zug ist: "+ aktuellerSpieler);
		
		
		//spieler vergleicht oberste karte am stapel passt mit einer handkarte
		Karte gefundeneKarte = aktuellerSpieler.passendeKarte(ablageStapel.get(0));
		
		if (gefundeneKarte !=null)
		{	System.out.println("karte ablegen " + gefundeneKarte);
			karteAblegen(gefundeneKarte);
		} else
		{
			Karte abgehobeneKarte = abheben ();
			if(abgehobeneKarte.match(ablageStapel.get(0)))
			{
				System.out.println("ablegen:" + abgehobeneKarte);
				karteAblegen(abgehobeneKarte);
			} else
			{
				System.out.println("karte aufnehmen");
				aktuellerSpieler.aufnehmen(abgehobeneKarte);
			}
		}
		
		if (aktuellerSpieler.anzahlDerHandkarten() == 0)
		{
			System.out.println("gewonnnen hat: " + aktuellerSpieler);
			return false;
		}
		//spielzug ende spieler objekt als letztes in die liste einf�gen
		mitspieler.add(aktuellerSpieler);
		return true;
	}
	
	public void austeilen()
	{	
		for (int zaehler = 0; zaehler < 7; zaehler++) 
		{	//jeden mitspieler einmal "besuchen"
			
			for (Spieler sp : mitspieler)
			{
				Karte karte = abheben();
				sp.aufnehmen(karte);
			}
		}
		
		Karte temp = abheben();
		ablageStapel.add(temp);
		System.out.println(temp);
	}
		
}
