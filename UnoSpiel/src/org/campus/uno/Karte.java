package org.campus.uno;

public class Karte
{ // eigenschaften der karte
	
	private Farbe farbe; // Farbe ist enum "Farbe" = Klasse Farbe.java
	private int zahl;
	
	public Karte (Farbe karten_farbe, int karten_zahl) // konstrucktor
	
	{
		farbe = karten_farbe;
		zahl = karten_zahl;
	}
	
	public boolean match(Karte passtDu)
	{
		if (farbe == passtDu.farbe || zahl == passtDu.zahl)
			{
			return true;
			}
		else
			{
			return false;			
			}
	}
	
	public boolean compare(Karte zweiteKarte)
	{			
		if (farbe == zweiteKarte.farbe) // wenn die farben gleich sind
		{
			//zahlen vergleichen
			boolean result = zahl > zweiteKarte.zahl;
			return result;
			
			// return zahl > zweiteKarte.zahl; // kurze schreibweise
		} 
		else 
		{	//ordinal gibt einen zahlenwert mit der stelle enum zur�ck
			return farbe.ordinal() < zweiteKarte.farbe.ordinal();
		}
	}
	
	public String toString()
	{
			return " " + farbe + " " +zahl;
	}
}
