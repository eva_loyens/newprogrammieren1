import org.campus.uno.UnoSpiel;
import org.campus.uno.Karte;
import org.campus.uno.Spieler;

public class UnoApp
{
	public static void main(String[] args)
	{
		UnoSpiel meinSpiel = new UnoSpiel();
		
		Spieler sp1 = new Spieler("Maverick");
		Spieler sp2 = new Spieler("Annabel");
		Spieler sp3 = new Spieler("Zane Cooper");
		Spieler sp4	= new Spieler ("Ich");
		
		meinSpiel.mitSpielen(sp1);
		meinSpiel.mitSpielen(sp2);
		meinSpiel.mitSpielen(sp3);
		meinSpiel.mitSpielen(sp4);

		meinSpiel.austeilen();
	
		int z�hler = 1; // spielz�ge mitz�hlen
		while (meinSpiel.spielzug())
		{
			System.out.println(z�hler + " ");
			z�hler++;
		}
		
	// Karte abgehobeneKarte = meinSpiel.abheben();
	// System.out.println(abgehobeneKarte);
	// System.out.println(meinSpiel.abheben());
		 
		
	}
}
