package org.campus.loop;
public class LoopApp
{
	public static void main (String[] args)
	
	{ 	//aufgabe: summe der reihe (br�chen van 1/2, 173, 1/4 ... bis 1/100)
		
		// double calculation1 = summeDerBrueche(100);
		// System.out.println("Die Summe der Br�che ergibt: " + calculation1);
		// --> diese 2 zeilen sind das gleiche als was nhier unterhalb in einer zeile geschrieben ist
		
		System.out.println("Die Summe der Br�che ergibt: " + summeDerBrueche(100));
		System.out.println();
	
		// aufgabe: summe der reihe (3 + 6 + 9 + 12 + 15 +... bis 9000)
	
		// int calculation2 = summeDerReihe(9000);
		// System.out.println("Die Summe der Reihe ergibt: " + calculation2);
		// --> diese 2 zeilen sind das gleiche als was hier unterhalb steht
		
		System.out.println("Die Summe der Reihe ergibt: " + summeDerReihe(9000));
		
	}
	
	public static double summeDerBrueche(int zahl1)
	
	{	
		double result = 0.0; 
		for (double index=2; index <= zahl1; index++)
		{
			result = result + (1 / index);
			// System.out.println(result);
		}
			
		return result;
	}
	
	public static int summeDerReihe(int zahl2)
	
	{	
		int result = 0;
		for (int index=3; index <= zahl2; index=index+3)
		{
			result = result + index;
			// System.out.println(result);
		}
		
		return result;
	}
}
