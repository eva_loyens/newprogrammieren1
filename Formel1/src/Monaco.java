
public class Monaco
{

	public static void main(String[] args)
	{
		Auto auto1 = new Auto("rot");
		Auto auto2 = new Auto("blau");
		Auto auto3 = auto1; // auto3 ist auto1 weil kein new steht
		
		auto1.status();
		auto2.status();
		auto3.status();
		System.out.println();
		
		auto1.volltanken();
		auto2.volltanken();
		auto1.status();
		auto2.status();
		System.out.println();
		
		auto1.beschleunigen(10);
		auto2.beschleunigen(60);
		auto3.beschleunigen(20);
		auto1.status();
		auto2.status();
		auto3.status();
		System.out.println();
		
		auto1.fahren(120);
		auto2.fahren(12000);
		auto3.fahren(400);
		auto1.status();
		auto2.status();
		auto3.status();
		
	}
	
}
