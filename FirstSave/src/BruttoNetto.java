
public class BruttoNetto
{
	public static void main(String[] args)
	{
		double netto = 150;
		double steuer = 20;
		double brutto = netto * (1 + steuer / 100);

		System.out.println(brutto);
		System.out.printf("Bruttopreis: %2f \n", brutto); // \n Zeilenumbruch
		System.out.printf("	Netto: %2f \n", netto); // \n Zeilenumbruch
		System.out.printf("	Steuer: %2f \n", steuer); // \n Zeilenumbruch

		int a = 54; // = ist Zuweisung!
		if (a == 2) // if braucht immer einen boolean
		{
			System.out.println("a ist gleich 2");
		} else if (a == 3) // if braucht immer einen boolean, == Vergleich
		{
			System.out.println("a ist gleich 3");
		} else
		{
			System.out.println("a ist nicht gleich 2 und nicht gleich 3");
		}

	}
}