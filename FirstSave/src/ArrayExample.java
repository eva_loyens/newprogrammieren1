
public class ArrayExample
{
	public static void main(String [] args)

	{	
		int[] feld1 = new int []
		{5, 2, 4, 1, 3, 8, 6, 2, 3, 4, 6};
		
		int stelle = 4;
		System.out.println(feld1[stelle]); // index aus einer variable
		System.out.println(feld1[3]); // wert an der stelle 3
		System.out.println( );
		
		System.out.println(feld1.length); // l�nge vom feld
		System.out.println();
		
		printArray(feld1);
		System.out.println();		
		ArrayMax.findMax(feld1); // verweis auf eine andere Klasse
		
		printArray(new int[] 
		{1, 2, 3, 4, 5, 6, 7});
		
		System.out.println();
		
		printArray(feld1); // ausgabe vor dem tauschen
		System.out.println();
		swap(feld1, 6, 1); // tauschen -> wir tauschen 6 und 1 // das geben wir von aussen vor 
		printArray(feld1); // nach dem tauschen
		System.out.println();
	}
	
	public static void printArray(int[] feld)
	{
		for (int index = 0; index < feld.length; index++) // alle inhalte aus dem feld
		{	
			System.out.printf(feld[index] + " ");
		}
	}
	
	// schreiben sie eine Methode die in einem Feld zwei Stellen vertauscht
	// die Methode hat das Feld und die erste und die zweite Stelle als Parameter
	
	public static void swap(int[] feld, int a, int b)
	{	
		int temporary = feld[a]; // wert an 1. stelle merken
		feld[a] = feld[b]; // wert an 2. stelle auf 1. stelle schreiben
		feld[b] = temporary; // gemerkten wert an die 1. stelle schreiben
	}
}
