
public class WhileExample
{

	public static void main(String[] args)
	{
		double einlage = 2000;
		double zinssatz = 1.025; // 2.5% zinsen
		double zielbetrag = 3000;
		double kontostand = einlage;
		
		int jahre=0;
		
		while (kontostand < zielbetrag)
		{	
			jahre++; // oder jahre = jahre +1;
			kontostand = kontostand * zinssatz;
			System.out.printf("%d. Jahr: %.2f \n",jahre, kontostand); // \n zeilenumbruch
		}
		
		System.out.println(jahre + " Jahre");
	}
		
		

}
