
public class TypKonvertierung
{

	public static void main(String[] args)
	{
		int intWert = 10;
		double doubleWert = 20.0;
	
		doubleWert = intWert; // int auf double zuweisen geht
		intWert = (int) doubleWert;
		
		String zeichenkette = "1234" + "1234";
		intWert = Integer.valueOf(zeichenkette);
		doubleWert = Double.valueOf(zeichenkette);
		System.out.println(zeichenkette);
		System.out.println();
		System.out.println(intWert);
		System.out.println();
		System.out.println(doubleWert);
	}

}
