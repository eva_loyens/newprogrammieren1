
public class BruttoNettoKategorie
{
	public static void main(String[] args)

	// string: braucht stringkette
	// datentyp
	// r�ckgabewert namen (frei w�hlen, klein geschrieben, camelcase)
	// void heisst kein r�ckgabewert

	{
		double nettoPreis = 2500000.00;
		double brutto = nettoPreis;
		int steuerKategorie = 1; // 2 oder 3

		brutto = bruttoPreis(100, 2); // aufrufen der Methode
		System.out.println(brutto);
		brutto = bruttoPreis(200, 1);
		System.out.println(brutto);

		// da kommt jetzt das IF...

		if (steuerKategorie == 1) // if braucht immer einen boolean
		{
			brutto = nettoPreis * 1.2; // 20% Steuer

		} else if (steuerKategorie == 2)
		{
			brutto = nettoPreis * 1.12; // 12% Steuer

		} else if (steuerKategorie == 3)
			brutto = nettoPreis * 1.1; // 10% Steuer

		System.out.println(brutto);

	}

	public static double bruttoPreis(double nettoPreis, int kategorie)

	// variable nettoPreis gildt n�r in {}
	{
		double brutto;
		switch (kategorie)
		{
		case 1:
			brutto = nettoPreis * 1.2; // 20% Steuer
			break;
		case 2:
			brutto = nettoPreis * 1.12; // 12% Steuer
			break;
		case 3:
			brutto = nettoPreis * 1.1; // 10% Steuer
			break;
		default:
			brutto = nettoPreis;
			break;
		}
		return brutto; // return liefert den wert zur�ck
	}

	public static double nettoPreis(double bruttoPreis, int kategorie)

	{
		double netto;
		switch (kategorie)
		{
		case 1:
			netto = bruttoPreis / 1.2;
			break;
		case 2:
			netto = bruttoPreis / 1.12;
			break;
		case 3:
			netto = bruttoPreis / 1.1;
			break;
		default:
			netto = bruttoPreis;
			break;
		}
		return netto; // return liefert den netto wert zur�ck
	}
}