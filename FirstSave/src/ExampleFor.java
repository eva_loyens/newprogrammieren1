
public class ExampleFor
{
	public static void main (String[] args)
	{
		printNumbers (50);
		printNumbers (5);
		
		int sum = sumUpNumbers(30);
		System.out.println(sum);
		
		System.out.println(sumUpNumbers(40)); // Ergebnis einer Methode gleich ausgeben
		
		System.out.println(sumUpNumbers(10));
		System.out.println(sumUpHalf(10));
		System.out.println(sumUpFraction(10));
		
	}
	
	public static int sumUpNumbers(int count)
	{
		int result = 0;
		for (int index = 1; index <= count; index++)
		{
			result = result + index;
		}
		
		return result;
	}
	
	public static void printNumbers(int counter)
	
	{
		for (int index = 0; index < counter; index ++)
		{
			System.out.println(index);
		}
	
	}
	public static double sumUpHalf (int count)
	
	{
		double result = 0;
		for (int index = 1; index <= count; index++)
		{
			result = result + index / 2;
		}
		
		return result;
	}
	
	public static double sumUpFraction (int count)
	
	{
		double result = 0;
		for (double index = 2; index <= count; index++)
		{
			result = result + (1 / index);
		}
		
		return result;
		
	}
 	
}

