
public class ExampleForEach
{
	public static void main(String[] args)
	{
		int [] zahlen = new int[]
				{1, 3, 5, 1, 7, 22, 33, 42};
	
		// name der variable (wert) : feld das ich durchlaufen will (zahlen)
		for (int wert : zahlen) // die schleife wird so oft durchlaufen wie das feld lang ist
		{
			System.out.println(wert); // bei jedem durchlauf hat wert den zahlen wert an der stelle im feld
	
	//	}
		// oben ist gleich wie das hier unten // oben for-each-schleife (verkurzte schleife)
		// unten ist for-schleife (ist l�ngere schleife)
		// for (int stelle = 0; stelle < zahlen.length; stelle++)
		// {
		//	int wert = zahlen[stelle];
		//	System.out.println(wert);
	//	}	
		}
		System.out.println();
		
		int summe = 0;
		for (int zahlenwert : zahlen)
		{
			summe = summe + zahlenwert;
		}
		System.out.println("Die Summe ist: " + summe);
	}
}