
public class ArrayMax
{
	public static void main (String[] args)
	{
		int[] a = new int[]
		{ 2, 3, 5, 1, 3, 7, 10, 23, 1 };
		
		int max= findMax(a);
		System.out.println(max);
	}
	
	public static int findMax(int[] feld)
	{
		int maximum = feld[0]; // das erste maximm ist der erste wert aus dem feld
		for (int stelle = 0; stelle < feld.length; stelle++)
		{
			if (maximum < feld[stelle])
			{
				maximum = feld [stelle];
			}
		}
		return maximum;
	}
}
