package org.campus.konto;

public class KontoApp

{
	public static void main (String[] args)
	{
		System.out.println("Sie haben nach 10 Jahren: " + kapitalMitZinsen(2000, 10, 1.5) + " Euro");
	}
	
	public static double kapitalMitZinsen (double kapital, int jahre, double zinsen)
	
	{
		double result = kapital;
		for (int index=1; index<=jahre; index++)
		{
			result = result * (1 + zinsen/100);
			System.out.println(" " + index);
			System.out.println("Kapital " + kapital + " Euro auf " + jahre + " Jahre ");
			System.out.println(zinsen + " % Zinsen = "  + result + " Euro");
			System.out.println();
		
		} 
		return result;
	}
}
