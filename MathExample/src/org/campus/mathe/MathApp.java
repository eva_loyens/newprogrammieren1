package org.campus.mathe;

public class MathApp

{
	public static void main (String[] args)
	{
		ziffernSumme(2566);
		
	}
	
	public static void ziffernSumme(int zahl)
	{	
		int summe = 0;
		int ziffer = 0;
		
		while (zahl > 0) // oder while (!0 = zahl)  
		{
			ziffer = zahl % 10; 
			summe = summe + ziffer;
			zahl = zahl / 10;
		}
		
		System.out.println(summe);
	}
}