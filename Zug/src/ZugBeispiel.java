
public class ZugBeispiel
{
	public static void main(String[] args)
	
	{
		Zug meineEisenbahn = new Zug();
		
		meineEisenbahn.neuerWagon("Schokolade");
		meineEisenbahn.neuerWagon("Kekse");
		meineEisenbahn.neuerWagon("Eis");
		meineEisenbahn.neuerWagon("Popcorn");
		meineEisenbahn.neuerWagon("Milkshake");
		meineEisenbahn.neuerWagon("Cola & Fanta");
		
		System.out.println(meineEisenbahn);
		System.out.println();
		System.out.println(meineEisenbahn.length());
		System.out.println();
		System.out.println(meineEisenbahn.wagonInhaltAnStelle(4));
	}
}
