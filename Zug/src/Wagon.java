
public class Wagon
{
	private String ladung;
	private Wagon nachbar;
	
	public Wagon(String inhalt)
	{
		ladung = inhalt;
	}
	
	public void einhaengen(Wagon neuerNachbar)
	{
		nachbar = neuerNachbar;
	}
	
	public String toString()
	{
		return "Wagon mit: " + ladung;
	}
	
	public Wagon nachbar()
	{
		return nachbar;
	}
}
