
public class Zug
{
	private Wagon erster;
	private Wagon letzter;
	private int zuglaenge = 0;
	
	public Zug()
	{
		
	}
	public void neuerWagon(String inhalt) 
	{
		Wagon neuerWagon = new Wagon(inhalt);
		// der zug ist leer
		
		if(erster == null) // null --> die referenz zeigt noch ins leere
		{
			erster = neuerWagon;
			letzter = neuerWagon;
		}
		else // es gibt zumindest schon einen wagon
		{
			letzter.einhaengen(neuerWagon); // punkt verbindet variabele die auf objekt "letzter"
			//zeigt mit methode einhaengen
			
			letzter = neuerWagon;
		}
		
		zuglaenge++;
	}	
		public String wagonInhaltAnStelle(int stelle)
		{	
			if (stelle > zuglaenge -1 || stelle < 0) // || heisst oder
			{
				return "fehler: Stelle nicht vorhanden";
			}
			
			Wagon temp = erster;
			
			for(int springen=0; springen < stelle; springen++)
			{
				temp = temp.nachbar(); // zum naechsten springen
			}
			return temp.toString();
		}
		
		public int length()
		{
			return zuglaenge;
		}
		
		public String toString()
		{
			if (erster == null)
			{
				return "0";
			}
			else
			{
				String ergebnis = zuglaenge + ":";
				
				Wagon temp = erster;
				while (temp != null)
				{
					ergebnis = ergebnis + " " + temp.toString();
					temp = temp.nachbar();
				}
				
				return ergebnis;
			}
		}
	}
