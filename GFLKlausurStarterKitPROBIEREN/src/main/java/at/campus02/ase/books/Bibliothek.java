package at.campus02.ase.books;

import java.util.ArrayList;


public class Bibliothek
{
	
	private ArrayList<Book> bibliothek = new ArrayList<Book>();

	public void addBook(Book b)
	{
		bibliothek.add(b);
	}

	public int countPages() 
	{	
		int summe=0;
		
		for (Book book : bibliothek) //foreach schleife
			// book ist eine variabele die nur innerhalb dieser methode verwendet wird
			// k�nnte auch poppie heissen z.B.
			// die foreach-schleife schleife geht alle elementen von der ArrayList bibliothek durch
			// WICHTIG: richtige Liste (in diesem Fall "bibliothek") durchsuchen!!
			// er geht alle elementen durch von der klasse Book in der Liste bibliothek
			// book ist eine temporaire variable die dazu dient etwas auszuf�hren (in diesem fall die seiten z�hlen)
			{
			summe = summe + book.getSeiten(); // getSeiten haben wir aus Book.java gelesen!
			}
		return summe;
	}

	public double avaragePages()
	{
		return (double) countPages()/bibliothek.size(); // die () klammern sind wichtig!! und nicht zu ubersehen
		//wenn du die methode .size von der ArrayList benutzen m�chtest
	}
	
	public ArrayList<Book> booksByAuthor(String autor)
	{	
		ArrayList<Book> autorenListe = new ArrayList<Book>(); // !!klammern () am ende der methode ArrayList<Book>();
		
			if (autor == null)
				return autorenListe;
			
			for (Book book : bibliothek) // foreach schleife
				// variabele book idem wie oben --> sehe countPages/getSeiten
			{ 	if (book.getAutor() == autor)
				autorenListe.add(book);
			}
				return autorenListe;
	}

	public ArrayList<Book> findBook(String titel)
	{
		// DAS WAR NICHT DIE AUFGABE!!
		// ArrayList<Book> titelListe = new ArrayList<Book>();
			
		//	if (titel == null)
		//		return titelListe;
			
		//	for (Book book : bibliothek)
		// {	
		//		if (book.getTitel() == titel)
		//	titelListe.add(book);
		// }
			
		//	return titelListe;
		
		//
		// hier muss methode eingef�gt werden aus Book.java
		
		ArrayList<Book> titelListe = new ArrayList<Book>();
		
		if (titel == null)
			// "null" schreiben weil String, dann kann keine 0 rauskommen
			// er gibt eine leere titelListe aus
		{
				return titelListe;
		}
		else
		{
			for (Book book : bibliothek) // foreach schleife
			{
				if (book.match(titel) == true)
				{	
					titelListe.add(book);
				}
			}
			return titelListe;
		}
	}
}
