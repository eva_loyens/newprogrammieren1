package at.campus02.ase.forum;

import java.util.ArrayList;

public class Forumseintrag

{	private String titel;
	private String text;
	private ArrayList <Forumseintrag> forum = new ArrayList<Forumseintrag>();

	public Forumseintrag(String titel, String text)
	{
		this.titel = titel;
		this.text = text;
	}

	public Forumseintrag antworten(String titel, String text) 
	{	
		Forumseintrag neuerEintrag = new Forumseintrag(titel, text);
		forum.add(neuerEintrag);
		
		return neuerEintrag;
	}

	public ArrayList<Forumseintrag> getAntworten() 
	{
		return forum;
	}

	public int anzahlDerEintraege() 
	{	
		int anzahl = 0;
		for (Forumseintrag neuerEintrag : forum )
		{
			anzahl = anzahl + 1 + neuerEintrag.forum.size();
			
			// + 1 weil er den ursprunglichen eintrag dazu z�hlt
			// dann komt mann auf 7 und nicht 6
		}
		
		return anzahl;		
	}
	
	public String toString()
	{
		
		if neuerEintrag.forum (!= null)
		{
		return String.format("[%s,%s]", titel, text);
		}
}
