package edu.campus02.fruechte;

public class Frucht
{	
	private String sorte;
	private double gewicht;

	public Frucht(String name, double gewicht)
	{
		this.sorte = name;
		this.gewicht = gewicht;
	}

	public double getGewicht()
	{
		return gewicht;
	}

	public String getSorte()
	{
		return sorte;
	}
	
	public String toString()
	{
		return String.format("(%s %.2f)", sorte, gewicht);
	}

}
