package edu.campus02.fruechte;

import java.util.ArrayList;

public class ObstKorb
{	
	ArrayList<Frucht> korb = new ArrayList<Frucht>();

	public void fruchtAblegen(Frucht f)
	{
		korb.add(f);
	}

	public int zaehleFruechte(String sorte)
	{
		int summe = 0;
		for (Frucht frucht : korb)
			if(frucht.getSorte().equals(sorte))
			{
				summe = summe + 1;
			}		
		return summe;
	}

	public Frucht schwersteFrucht()
	{
		Frucht schwerste = korb.get(0);
		for (Frucht frucht : korb)
		{
			if (schwerste.getGewicht() < frucht.getGewicht())
				schwerste = frucht;
		}		
		return schwerste;
	}

	public ArrayList<Frucht> fruechteSchwererAls(double gewicht)
	{
		ArrayList<Frucht> vergleich = new ArrayList<Frucht>();
		for (Frucht frucht : korb)
			{
			if (frucht.getGewicht()> gewicht)
				vergleich.add(frucht);
			}		
		return vergleich;
	}

	public int zaehleSorte()
	{
		ArrayList<String> obstSorte = new ArrayList<String>();
		for (Frucht frucht : korb)
		{
			if (!obstSorte.contains(frucht.getSorte()))
				obstSorte.add(frucht.getSorte());
		}
			
		return obstSorte.size();
	}
	
	public String toString()
	{
		return korb.toString();
	}
}
