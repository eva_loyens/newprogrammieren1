package edu.campus02.dosen;
import java.util.ArrayList;

public class Keksdose

{	private String farbe;
	private double breite;
	private double laenge;
	private double hoehe;
	private ArrayList<Keks> kekse = new ArrayList<Keks>();
	
	public Keksdose(String farbe, double x, double y, double hoehe)
	{
		this.farbe = farbe;
		this.breite = x;
		this.laenge = y;
		this.hoehe = hoehe;
	}

	public double getBreite()
	{
		return breite;
	}

	public double getLaenge()
	{
		return laenge;
	}

	public double getHoehe() {
		return hoehe;
	}

	public double volumen() 
	{
		return (breite*laenge*hoehe);
	}

	public double freiesVolumen()
	{
		double frei = 0.0;
		{
			frei = volumen() - (keks.getVolumen());
		}
		return frei;
	}

	public boolean keksAblegen(Keks keks)
	{
		if (freiesVolumen().isEmpty())
			
			for (Keks keks : kekse)
		{
			keksAblegen.add(keks);
		}
		return true;
	}

	public boolean leer()
	{
		return false;
	}

	public boolean stapeln(Keksdose d)
	{
		double volumen1;
		double volumen2;
		
		if (volumen1 < volumen2)
		{
		}

		return true;
	}

	public double gesamtVolumen()
	{
		return 0;
	}
	
	public String toString()
	{
		return String.format("(%s %.1f x %.1f x %.1f)", farbe, breite, laenge, hoehe);
	}
}
